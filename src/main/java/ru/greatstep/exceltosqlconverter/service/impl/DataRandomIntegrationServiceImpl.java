package ru.greatstep.exceltosqlconverter.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.greatstep.exceltosqlconverter.models.FakeName;
import ru.greatstep.exceltosqlconverter.service.DataRandomIntegrationService;
import ru.greatstep.exceltosqlconverter.utils.WebClientHelper;

@Service
@RequiredArgsConstructor
public class DataRandomIntegrationServiceImpl implements DataRandomIntegrationService {

    private final WebClientHelper webClientHelper;
    private final ObjectMapper objectMapper;

    @Value("${external-api.random-data-tools}")
    private String host;

    private static final Integer MAX_COUNT = 100;

    @Override
    public List<FakeName> getFakeNames(Integer count, List<String> params) {
        count = count == null || count == 0 ? 1 : count;
        if (count <= MAX_COUNT) {
            return getFakeNamesFromApi(count, params);
        } else {
            List<FakeName> fakeNamesFromApi = new ArrayList<>();
            while (count > 0) {
                var fakeNames = getFakeNamesFromApi(count, params);
                fakeNamesFromApi.addAll(fakeNames);
                count -= fakeNames.size();
            }
            return fakeNamesFromApi;
        }
    }

    private List<FakeName> getFakeNamesFromApi(Integer count, List<String> params) {
        return count == 1 ? getSingletonFake(params) : getListFake(count, params);
    }

    @SneakyThrows(JsonProcessingException.class)
    private List<FakeName> getSingletonFake(List<String> params) {
        var response = getFake(JsonNode.class, getParamsWithoutCount(params));
        var fakeName = objectMapper.treeToValue(response, FakeName.class);
        return List.of(fakeName);
    }

    @SneakyThrows(JsonProcessingException.class)
    private List<FakeName> getListFake(Integer count, List<String> params) {
        var response = Optional.ofNullable(getFake(ArrayNode.class, getParams(count, params))).orElseThrow();
        Iterator<JsonNode> itr = response.elements();
        List<FakeName> fakeNames = new ArrayList<>();
        while (itr.hasNext()) {
            fakeNames.add(objectMapper.treeToValue(itr.next(), FakeName.class));
        }
        return fakeNames;
    }

    private <T> T getFake(Class<T> respClass, Map<String, Object> params) {
        return webClientHelper.getRequest(host, params, respClass).block();
    }

    private Map<String, Object> getParamsWithoutCount(List<String> params) {
        return Map.of(
                "type", "all",
                "params", String.join(",", params)
        );
    }

    private Map<String, Object> getParams(Integer count, List<String> params) {
        return Map.of(
                "type", "all",
                "count", count > MAX_COUNT ? MAX_COUNT : count,
                "params", String.join(",", params)
        );
    }

}
