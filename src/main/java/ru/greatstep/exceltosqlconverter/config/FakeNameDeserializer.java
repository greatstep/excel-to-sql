package ru.greatstep.exceltosqlconverter.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import lombok.SneakyThrows;
import ru.greatstep.exceltosqlconverter.models.FakeName;

public class FakeNameDeserializer extends JsonDeserializer<FakeName> {

    @Override
    @SneakyThrows({IOException.class})
    public FakeName deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        JsonNode node = jsonParser.readValueAsTree();
        return new FakeName(
                node.get("LastName") != null ? node.get("LastName").asText() : null,
                node.get("FirstName") != null ? node.get("FirstName").asText() : null,
                node.get("FatherName") != null ? node.get("FatherName").asText() : null,
                node.get("DateOfBirth") != null ? node.get("DateOfBirth").asText() : null,
                node.get("YearsOld") != null ? node.get("YearsOld").asInt() : null,
                node.get("Phone") != null ? node.get("Phone").asText() : null,
                node.get("Login") != null ? node.get("Login").asText() : null,
                node.get("Password") != null ? node.get("Password").asText() : null,
                node.get("Email") != null ? node.get("Email").asText() : null,
                node.get("Gender") != null ? node.get("Gender").asText() : null,
                node.get("GenderCode") != null ? node.get("GenderCode").asText() : null,
                node.get("PasportNum") != null ? node.get("PasportNum").asText() : null,
                node.get("PasportSerial") != null ? node.get("PasportSerial").asText() : null,
                node.get("PasportNumber") != null ? node.get("PasportNumber").asText() : null,
                node.get("PasportCode") != null ? node.get("PasportCode").asText() : null,
                node.get("PasportOtd") != null ? node.get("PasportOtd").asText() : null,
                node.get("PasportDate") != null ? node.get("PasportDate").asText() : null,
                node.get("inn_fiz") != null ? node.get("inn_fiz").asText() : null,
                node.get("inn_ur") != null ? node.get("inn_ur").asText() : null,
                node.get("snils") != null ? node.get("snils").asText() : null,
                node.get("oms") != null ? node.get("oms").asText() : null,
                node.get("ogrn") != null ? node.get("ogrn").asText() : null,
                node.get("kpp") != null ? node.get("kpp").asText() : null,
                node.get("Address") != null ? node.get("Address").asText() : null,
                node.get("Country") != null ? node.get("Country").asText() : null,
                node.get("Region") != null ? node.get("Region").asText() : null,
                node.get("City") != null ? node.get("City").asText() : null,
                node.get("Street") != null ? node.get("Street").asText() : null,
                node.get("House") != null ? node.get("House").asText() : null,
                node.get("Apartment") != null ? node.get("Apartment").asText() : null,
                node.get("bankBIK") != null ? node.get("bankBIK").asText() : null,
                node.get("bankCorr") != null ? node.get("bankCorr").asText() : null,
                node.get("bankInn") != null ? node.get("bankInn").asText() : null,
                node.get("bankKpp") != null ? node.get("bankKpp").asText() : null,
                node.get("bankNum") != null ? node.get("bankNum").asText() : null,
                node.get("bankClient") != null ? node.get("bankClient").asText() : null,
                node.get("bankCard") != null ? node.get("bankCard").asText() : null,
                node.get("bankDate") != null ? node.get("bankDate").asText() : null,
                node.get("bankCvc") != null ? node.get("bankCvc").asText() : null,
                node.get("EduSpeciality") != null ? node.get("EduSpeciality").asText() : null,
                node.get("EduProgram") != null ? node.get("EduProgram").asText() : null,
                node.get("EduName") != null ? node.get("EduName").asText() : null,
                node.get("EduDocNum") != null ? node.get("EduDocNum").asText() : null,
                node.get("EduRegNumber") != null ? node.get("EduRegNumber").asText() : null,
                node.get("EduYear") != null ? node.get("EduYear").asText() : null,
                node.get("CarBrand") != null ? node.get("CarBrand").asText() : null,
                node.get("CarModel") != null ? node.get("CarModel").asText() : null,
                node.get("CarYear") != null ? node.get("CarYear").asText() : null,
                node.get("CarColor") != null ? node.get("CarColor").asText() : null,
                node.get("CarNumber") != null ? node.get("CarNumber").asText() : null,
                node.get("CarVin") != null ? node.get("CarVin").asText() : null,
                node.get("CarSts") != null ? node.get("CarSts").asText() : null,
                node.get("CarStsDate") != null ? node.get("CarStsDate").asText() : null,
                node.get("CarPts") != null ? node.get("CarPts").asText() : null,
                node.get("CarPtsDate") != null ? node.get("CarPtsDate").asText() : null,
                getFullName(node)
        );
    }

    private String getFullName(JsonNode node) {
        return String.join(" ",
                node.get("FirstName") != null ? node.get("FirstName").asText() : null,
                node.get("FatherName") != null ? node.get("FatherName").asText() : null,
                node.get("LastName") != null ? node.get("LastName").asText() : null);
    }

}
