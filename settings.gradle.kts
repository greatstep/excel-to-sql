pluginManagement {
    repositories {
        maven {
            url = uri("http://nexus-maven.greatstep.ru/repository/maven-public/")
            isAllowInsecureProtocol = true
        }
        maven {
            url = uri("http://nexus-maven.greatstep.ru/repository/greatstep-maven-releases/")
            isAllowInsecureProtocol = true
        }
    }
}
rootProject.name = "excel-to-sql-converter"
