buildscript {
    repositories {
        maven {
            url = uri("http://nexus-maven.greatstep.ru/repository/maven-public/")
            isAllowInsecureProtocol = true
        }
        maven {
            url = uri("http://nexus-maven.greatstep.ru/repository/greatstep-maven-releases/")
            isAllowInsecureProtocol = true
        }
    }
}

plugins {
    java
    id("org.springframework.boot") version "3.1.0"
    id("io.spring.dependency-management") version "1.1.4"
    id("jacoco")
    id("checkstyle")
}

group = "ru.greatstep"
version = "1.0.1"

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

configurations {
    compileOnly {
        extendsFrom(configurations.annotationProcessor.get())
    }
}

repositories {
    maven {
        url = uri("http://nexus-maven.greatstep.ru/repository/maven-public/")
        isAllowInsecureProtocol = true
    }
    maven {
        url = uri("http://nexus-maven.greatstep.ru/repository/greatstep-maven-releases/")
        isAllowInsecureProtocol = true
    }
}

val mapstructVersion: String = "1.5.5.Final"

dependencies {
    //SPRING
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    //OTHER
    implementation("org.postgresql:postgresql:42.6.0")
    implementation("org.apache.poi:poi:5.2.5")
    implementation("org.apache.poi:poi-ooxml:5.2.5")
    implementation("org.apache.poi:poi-ooxml-schemas:4.1.2")
    implementation("org.dhatim:fastexcel:0.15.4")
    implementation("org.dhatim:fastexcel-reader:0.16.4")
    implementation("org.apache.commons:commons-lang3:3.14.0")

    compileOnly("org.projectlombok:lombok")
    annotationProcessor("org.projectlombok:lombok")
    implementation("org.mapstruct:mapstruct:${mapstructVersion}")
    compileOnly("org.mapstruct:mapstruct-processor:${mapstructVersion}")
    annotationProcessor("org.mapstruct:mapstruct-processor:${mapstructVersion}")
    //TEST
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("com.squareup.okhttp3:mockwebserver:4.12.0")
    testImplementation("com.squareup.okio:okio-jvm:3.8.0")
}

tasks.test {
    useJUnitPlatform()
    finalizedBy("jacocoTestReport")
}

jacoco {
    toolVersion = "0.8.8"
}

tasks.getByName<JacocoReport>("jacocoTestReport") {
    reports {
        xml.required.set(true)
        csv.required.set(true)
    }
    afterEvaluate {
        classDirectories.setFrom(files(classDirectories.files.map {
            fileTree(it) {
                exclude(
                        "ru/rtech/config/**",
                        "ru/rtech/exception/**",
                        "ru/rtech/controller/advice/**",
                        "ru/rtech/model/**",
                        "ru/rtech/repository/**",
                        "ru/rtech/util/**",
                        "ru/rtech/Application*"
                )
            }
        }))
    }
}

tasks.processResources {
    filesMatching("application.yml") {
        expand(project.properties)
    }
}

tasks.bootJar {
    archiveFileName.set("ex.jar")
}

checkstyle {
    toolVersion = "10.13.0"
    config = project
            .resources
            .text
            .fromInsecureUri(uri("http://nexus-maven.greatstep.ru/repository/binary-public/checkstyle/checkstyle.xml"))
}

tasks.withType<Checkstyle>().configureEach {
    reports {
        xml.required.set(false)
        html.required.set(true)
        html.stylesheet = project
                .resources
                .text
                .fromInsecureUri(uri("http://nexus-maven.greatstep.ru/repository/binary-public/xsl/checkstyle-simple.xsl"))

    }
}